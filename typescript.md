# Documentaci�n sobre Typescript

**Comandos b�sicos para la consola:** 

```
1. cd ruta_del_archivo
2. tsc -w *.ts

```

---


**Tipos de datos principales:** 

```
Boolean -------> let casado: boolean = false;
Number -------> let edad: number = 25;
String -------> let color: string = "blue";
Array -------> let comprar: string[] = ["patatas", "lechuga", "pan"];
Any -------> let noEstoySeguro: any = 4;

```

---


**Los Templates :** 

```
Usamos ``:


var frase = "pan y leche";

var html = `<div>Tengo que comprar ${frase}</div>`;

```


---


**Tipos de parametros en las funciones :** 

```
                obligatorio        default value           opcional
function sumar(numero1: number, numero2: number = 23, numero3?: number) {

    if(!numero3){
    
       var resultado = numero1 + numero2;
    
    } else {
    
       var resultado = numero1 + numero2 + numero3;
    
    }

    return resultado;
    
}

```

---


**Los objetos :** 

```

person = {

    name: "Pepe",
    surname: "Domingues",
    age: 32
  };

```

Para definir par�metros comunes, creamos una interfaz:

```
export interface Persona {

    name: string;
    age: number;
    address: any[];
    getSalary: () => any;

}
```


```
private person: Persona = {

    name: "Pepe";
    age: 25;
    address: ["Valle Inclan", 22];
    getSalary: (){
    
        return this.age + " a�os y 1200�";

    }

  };
```


---


**Ciclo FOR OF :** 

```

for ( const person of this.people){

      console.log(person.name);

    }
    
```    
    
    
---

**Funciones de flecha / Arrow Functions:** 


> SIN FLECHA:

```

var sumar = function(numero1: number, numero2: number) {

 return numero1 + numero2;
 
}

```

> CON FLECHA:

```

let sumar = (a, b) => a + b; 

```

> CON VARIAS LINEAS DE CODIGO:

```

let sumar = (a, b) =>{
  
  return a + b;

};

```

> CON SET_TIME_OUT:

```


let sumar = {

    a : 3,
    b : 4,
    
    resultado(){
        
        setTimeout( () => console.log(this.numero1 + this.numero2), 1500 );
        
    }
        
}

sumar.resultado();


```

---


**Destructuraci�n de objetos y arrays:** 


> CON OBJETOS:

```

let persona = {

    nombre: "Diego",
    apellido: "Ruiz",
    telefono: 671727219
}


let {nombre, apellido, telefono} = persona;


console.log(nombre, apellido, telefono);


```

> CON ARRAYS:


```

let compra: string[] = ["pan", "leche", "tomates"];


let[lista1, lista2, lista3] = compra;


console.log(lista1, lista2, lista3);


```


---


**Las promesas ejecutan una tarea cuando otra se ha terminado:** 


```
var promesa = new Promise((resolve, reject) => {

    resolve('ok');
    reject('mal');
    
});

promesa.then((res) => {
    
    console.log('Todo va bien:', res === 'ok'); 
});

promesa.catch((err) => {
    
    console.log('Algo ha salido mal:', err === 'mal'); 
    
});

```



---


**Las clases ||** 
**Los constructores se ejecutan cuando se llama a la instancia de la clase:** 

```

class Persona {
    
    public nombre: string;
    private edad: number;
    protected casado: boolean;
    
    constructor(nombre, edad, casado){
        
        this.nombre = nombre;
        this.edad = edad;
        this.casado = casado;
    }
}

var resultado = new Persona("Diego", 25, false);

console.log(resultado);


```

** Para calcular el �rea de un tri�ngulo: **

```

class Rectangulo {
    
    base:number;
    altura:number;
 
    constructor(base:number , altura:number)
    {
      this.base = base;
      this.altura = altura;
    }
 
    getArea()
    {
      return this.base * this.altura;
    }
}
 
let rectangulo = new Rectangulo(2 , 6);

console.log(rectangulo.getArea());

```


---


**Los Decoradores ||** 
**Son simples funciones que complementan a una clase:** 


```

function calcularEdad(constructor: Function){
    
    console.log(constructor);
    
}


@calcularEdad
class Persona {
    
    public nombre: string = "Diego";
    
    constructor(public edad: number){
        
        
    }
    
}

```


---


**Las Interfaces ||** 
**Nos permiten agregar el tipo de dato por defecto para hacer m�s robusta la aplicaci�n:** 


```

interface Perro {

    nombre: string,       Indicamos el tipo de dato
    edad: string
    
};


const datosPersonales: Perro = {

   nombre: "Mascota",     Utilizamos los datos mas tarde
   edad: 2
   
};

```










































