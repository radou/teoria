# Diagrama de Casos de Uso


Diagrama principal, con **Actores** y **Escenarios**:


![img](https://posmarketpro.files.wordpress.com/2017/06/usecasediagram1.png?w=1040)


Subescenarios del diagrama principal:

    <<include>> Acción obligatoria e indispensable. Orden de las flechas de dentro a fuera.
    <<extends>> Acción secundaria a consecuencia de la principal. Orden de las flechas de fuera a dentro.
    
    
![img](https://posmarketpro.files.wordpress.com/2017/06/caso-de-estudio-subescenario-1-reservas.png?w=1040)

---

# Diagrama de Flujo/Actividades


** Con notas al inicio y al final del proceso **:


![img](https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/activity-diagram-for-login-UML/activity-diagram-for-login-UML-650x797.png)


---

# Diagrama de Secuencia

*Clases: Sustantivos, indican el nombre de la clase.*
*Atributos: Características de la clase, de diferentes tipos (String, Number, Boolean, Any, etc)*
*Métodos: Acción que realiza esa clase u objeto.*


![img](https://weizyapp.com/pruebainput/diagrama.png)


Aplicamos los elementos en diferentes celdas (Objetos o Clases) describiendo los pasos para el funcionamiento de la aplicación.

*Flecha sincrónica: Cabeza negra, requiere respuesta del proceso.*
*Flecha asincrónica: Cabeza simple, no requiere respuesta del proceso.*

![img](https://posmarketpro.files.wordpress.com/2017/06/sequencediagram1.png?w=1040)

---

# Diagrama de Clases

```

La multiplicidad entre clases:


 0 … 1

 1

 0 … * (cero o muchos)

 1 … *

 *

-------------------------------------------------

Entonces para este caso:

 1 Cliente realiza 1...* reservas 
 1 Agenda mantiene 0...* reservas 
 1 Taquilla utiliza 1 agenda 

```

![img](https://posmarketpro.files.wordpress.com/2017/06/diagrama-de-clases-main.jpg?w=1040)


** La herencia trata de una clase Madre o Principal y una clase Hija o Complementaria **
** Los relacionamos con una flecha hueca del hijo a la clase madre **


![img](https://posmarketpro.files.wordpress.com/2017/06/diagrama-de-clases-herencia.jpg?w=1040)

```

 Las propiedades de la clase:
 
     Acceso a las propiedades

        Público (+)
        Privado (-)
        Protegido (#)
        Paquete (~)
        Derivado (/)
        Estático (subrayado)
        
    Los tipos de propiedades (JavaScript)

        String
        Number
        Boolean
        Array
        Any
        Tuple
        Enum
        Void
        Never
        Object
        Tipos propios (inventados por nosotros)
        Date
        


```

![img](https://posmarketpro.files.wordpress.com/2017/06/diagrama-de-clases-atributos.jpg?w=1040)


```

 Finalmente, los métodos de una Clase: 
 
 SON LAS FUNCIONES QUE VAN DENTRO DE UNA CLASE 
 
 
 calcularPrecio()
 calcularPrecio(): number
 calcularPrecio(parametro de entrada)
 calcularPrecio(parametro de entrada): number
 
 
```


![img](https://posmarketpro.files.wordpress.com/2017/06/diagrama-de-clases-operaciones.jpg?w=1040)




















