# Instalar Sass

`npm install -g sass`

# Ejecutar compilador (Proyecto a parte)

1. Seleccionamos el directorio `cd desktop/miProyecto/scss`

2. `sass --watch *.scss output.css`

---

# Sass en Angular 

Creamos el proyecto con SASS

`ng new miNuevaApp --style=scss`


