# Documentación Angular


# Paso 1: Acceder a la carpeta para crear el proyecto

```
cd /desktop/miNuevaApp

```

# Paso 2: Crear un proyecto de Angular 

```
ng new miNuevaApp

```
Podemos crearlo con SASS 

`ng new miNuevaApp --style=scss`

# Paso 3: Volvemos a indicar posición de la carpeta en el sistema

```
cd /desktop/miNuevaApp

```

# Paso 4: Abrir/Servir la aplicación

```
ng serve --open

```

---

## Generar Componentes / Directorio completo: Ver arriba como enlazar para que se muestren

** Aunque tengamos o no la carpeta COMPONENTS creada SIEMPRE ES LO MISMO:**

```
ng generate component components/about

```

** Agregamos la etiqueta html en app.module.html **

`<app-about></app-about>

---

## Evento (click)

** El evento (click) se inserta en el HTML y permite ejecutar una función al hacer click en esta:**

`<button (click)="mostrarProductos()">Mostrar</button>`

---

## ngIf / ngFor

** ngIf permite mostrar u ocultar HTML dependiendo de un booleano TRUE / FALSE:**

`<li *ngIf="true">{{persona.nombre}}</li>`


** ngFor permite RECORER UN ARRAY para MOSTRARLO POR PANTALLA en el HTML:**

```
listaCompra: string[] = ["pan", "leche", "huevos"];

<ul>
    <li *ngFor="let valores of listaCompra; index as i"> {{ i }} {{ valores }}</li>
</ul>
```

---

## ngSwitch

```
<button (click)=" color= 'caso1' ">Cambiar</button><br><br>

<div [ngSwitch]="color">

    <div *ngSwitchCase=" 'caso1' " class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
    </div>

    <div *ngSwitchCase=" 'caso2' " class="alert alert-secondary" role="alert">
        A simple secondary alert—check it out!
    </div>

    <div *ngSwitchCase=" 'caso3' " class="alert alert-success" role="alert">
        A simple success alert—check it out!
    </div>

    <div *ngSwitchCase=" 'caso4' " class="alert alert-dark" role="alert">
        A simple success alert—check it out!
    </div>

    <div *ngSwitchDefault class="alert alert-danger" role="alert">
        A simple danger alert—check it out!
    </div>

</div>
```



---


## Las Rutas de Navegacion

** Creamos el archivo de rutas:**

`app.routes.ts`


** En el incluimos:**

```
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { HeroesComponent } from './components/heroes/heroes.component';


const APP_ROUTES: Routes = [

    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'heroes', component: HeroesComponent },
    { path: '**', redirectTo: 'home', pathMatch: 'full' }
   
  ];
  

  export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
```

** En app.module.ts incluimos:**


```
// Rutas

import { APP_ROUTING } from './app.routes';
```

`Más abajo, en imports: APP_ROUTING`


** En app.component.html incluimos:**

`<router-outlet></router-outlet>`


** Finalmente en el Navbar incluimos los enlaces a cada componente:**

```
<ul>
    <li> <a [routerLink]="['home']" [routerLinkActive]="['active']">Home</a> </li>
    <li> <a [routerLink]="['about']" [routerLinkActive]="['active']">About</a> </li>
    <li> <a [routerLink]="['heroes']" [routerLinkActive]="['active']">Heroes</a> </li>
</ul>
```


---


## Los Servicios


* PARTE A

** Creamos el archivo de rutas dentro de APP:**

`ng generate service services/heroes`


** Importamos el archivo en APP.MODULE.TS:**

```
// Servicios
import { HeroesService } from './services/heroes.service';

```

```
// En providers

 providers: [
    HeroesService
  ],

```

** Importamos el archivo en el componente necesario:**


heroes.component.ts -> `import { HeroesService } from '../../services/heroes.service';`

En el constructor -> 

```
constructor(private _heroesService:HeroesService) { 
    
}
```

* PARTE B

** Agregamos toda la informacion en el servicio para poder solicitarla despues:**


```
private heroes: any[] = [
      {
        nombre: "Aquaman",
        bio: "El poder más reconocido de Aquaman es la capacidad telepática para comunicarse con la vida marina, la cual puede convocar a grandes distancias.",
        img: "assets/img/aquaman.png",
        aparicion: "1941-11-01",
        casa:"DC"
      },
      {
        nombre: "Batman",
        bio: "Los rasgos principales de Batman se resumen en «destreza física, habilidades deductivas y obsesión». La mayor parte de las características básicas de los cómics han variado por las diferentes interpretaciones que le han dado al personaje.",
        img: "assets/img/batman.png",
        aparicion: "1939-05-01",
        casa:"DC"
      }
      
];

```

** Creamos la funcion getHeroes que decuelve el valor del array superior: **


```
 getHeroes(){
    return this.heroes;
  }

```

** Mostramos los valores del SERVICE en el COMPONENTE: **


Creamos un array vacio -> `heroes: any[] = [];`

La funcion ngOnInit() se ejecuta más tarde que el constructor: 

```
 ngOnInit() {

    this.heroes = this._heroesService.getHeroes();
    
  }

```

* PARTE C


** En el archivo SERVICES podemos crear una INTERFAZ con las propiedades para hacer MAS SEGURA la aplicacion: **

Con la palabra export

```
 export interface Heroe {

  nombre: string;
  bio: string;
  img: string;
  aparicion: string;
  casa: string;

}

```

** Indicamos en el array superior que en lugar de tipo ANY es de tipo HEROES: **


Cambiamos el tipo -> `heroes: Heroes[] = [];`


** Podemos importar la interfaz (SI LA NECESITAMOS) en el archivo heroes.component.ts: **

Porque ya se incluye en el archivo de :

`import { HeroesService, Heroes } from '../../services/heroes.service';`

Y cambiamos aqui tambien el tipo ANY por -> `heroes: Heroe[] = [];` 


---

## RECORRER Y MOSTRAR un ARRAY con ngFor

** Utilizamos ngFor e indicamos con las directivas la ruta dentro de las propiedades del array**

```
<ul *ngFor="let valor of heroes; index as i">
    <li> {{ i }} </li>
    <li> {{ valor.nombre }} </li>
    <li> {{ valor.casa }} </li>
</ul>
```

---

# Pasar parámetros/valores en la URL y que se muestren por pantalla

# Paso 1: Pasar valores por la URL


## Routerlink e ID / Pasando el ID por la URL

** En app.routes.ts indicamos el id del personaje para mostrarlo por pantalla: **

`{ path: 'personaje/:id', component: PersonajeComponent },`


** Despues, utilizamos RouterLink para acceder a la pagina PERSONAJE con id del heroe: **

```
<ul *ngFor="let valor of heroes; index as i">
    <li> {{ i }} </li>
    <li> {{ valor.nombre }} </li>
    <li> <a [routerLink]="['/personaje', i]"> Ver más </a> </li>
</ul>
```

## Funciones y button / Pasando el ID por la URL

Utilizamos el evento (click) con el parametro (i) de index:

`<button (click)="verHeroe(i)"> Mostrar </button>`

Creamos la función en el archivo de heroes.component.ts:

```
verHeroe(indice:number){

    this.router.navigate(['/personaje', indice]);

}

```

Importamos Router en la parte superior:

`import {Router} from '@angular/router';`


En los parametros del constructor agregamos:

`private router:Router`


# Paso 2: Mostrar los valores pasados en la URL por PANTALLA

Creamos el componente personaje.component.ts para mostrar por pantalla


En la seccion app.routes.ts indicamos los parametros:

`{ path: 'personaje/:id', component: PersonajeComponent },`
`{ path: 'personaje/:nombre/:apellidos', component: PersonajeComponent },`


En la seccion personaje.component.ts importamos

`import {ActivatedRoute} from '@angular/router'; `

y heroes.service.ts (DONDE TENEMOS EL GRAN ARRAY CON LA DATA)

`import { HeroesService } from '../../services/heroes.service';`

Dentro de la clase:

` personaje: any = {}; `

Y en el contructor:


```

constructor( private activatedRoute: ActivatedRoute,
               private _heroesService: HeroesService
    ){

    this.activatedRoute.params.subscribe( params =>{
    
        this.personaje = this._heroesService.getHeroe( params.id );

    });
}

```

Finalmente en heroes.service.ts creamos la funcion que devuelve los elementos del array con datos:

```
getHeroe(idx: string){
    return this.heroes[idx];
  }
  
```

Ya podemos mostrar los datos en el archivo personaje.components.html

```
<ul>
    <li> {{personaje.nombre}} </li>
    <li> {{personaje.bio}} </li>
    <li> <img src="{{personaje.img}}" alt="Smiley face" height="42" width="42"> </li>
    <li> {{personaje.aparicion}} </li>
    <li> {{personaje.casa}} </li>
</ul>
```

---

## Buscador en el navbar

Creamos el input +  el button
El evento (keyup.enter) se ejecuta al pulsar la tecla enter
buscarTexto es el identificador de la etiqueta
(click) ejecuta la misma funcion

```
<div class="topnav">
    <input type="text" (keyup.enter)="buscarHeroe(buscarTexto.value)" #buscarTexto>
    <button (click)="buscarHeroe(buscarTexto.value)" type="button">Buscar</button>
</div>
```

en el archivo header.component.ts:

```
buscarHeroe( termino: string ){

      this.router.navigate( ['/resultados', termino] );
}
```

En routes agregamos el enlazado del componente:

`import { ResultadosComponent } from './components/resultados/resultados.component';`
`{ path: 'resultados/:termino', component: ResultadosComponent },`

Creamos una funcion en el servicio para buscar dentro del array con la data:

```
buscarHeroes( termino: string ){
    
    let personajeArr: Heroe[] = [];
    termino = termino.toLocaleLowerCase();

    for (let personaje of this.heroes) {

      let nombre = personaje.nombre.toLocaleLowerCase();
      
      if(nombre.indexOf( termino ) >= 0 ) {

        personajeArr.push( personaje );
      }
    }

    return personajeArr;
  }
```
Finalmente en la pagina de reultados donde se mostraran:

Agregamos el servicio heroes que contiene la data y activated route:

```
import {ActivatedRoute} from '@angular/router';
import { HeroesService } from '../../services/heroes.service';
```

Dentro de la clase

```
personaje: any[] = [];
  termino:string;
  mensaje = "No se han encontrado coincidencias con";

  constructor( private activatedRoute: ActivatedRoute,
               private _heroesService:HeroesService,
               private _resultadosService: ResultadosService
              ) {}


  ngOnInit() {

    this.activatedRoute.params.subscribe( params =>{
      this.termino = params['termino'];
      this.personaje = this._heroesService.buscarHeroes( params['termino'] );

  });

  }
```

## Resumen: 

1. Creamos el COMPONENTE deseado
2. Creamos la RUTA para ese componente en app.routes.ts
   
Existen dos tipos de rutas (con parametros o sin parametros):

```
{ path: 'personaje/:id', component: PersonajeComponent },
{ path: 'personaje', component: PersonajeComponent },
```

3. En el componente importamos ActivatedRoute y continuamos abajo (Ver arriba)
4. En el navbar importamos Router y continuamos abajo (Ver arriba)
5. Importamos el servicio que queremos mostrar en el componente (Ver arriba)
6. Agregamos el HTML en la pagina del componente para que se muestre

---

## Generar Pipes personalizados

```
ng generate pipe pipes/capitalizado

```

En el componente capitalizado.pipe.ts desarrollamos la funcionalidad

```
export class CapitalizadoPipe implements PipeTransform {

  transform(value: any, color: boolean): any {

    if(color) {

      document.getElementById('prueba').style.color = 'red';

    }
    
    value = value.toLowerCase();

    return value;

  }

}

```

En el elemento html aplicamos el pipe 


`{{ nombre | capitalizado: true }}`


---

## Domseguro Pipe

** Este pipe personalizado, hace que se pueda mandar un parametro variable por la URL de forma segura: **

```
import { DomSanitizer } from '@angular/platform-browser';

constructor( private domSanitizer: DomSanitizer){}

  transform(value: string): any {
    
    const url = 'https://open.spotify.com/embed?uri=';
    return this.domSanitizer.bypassSecurityTrustResourceUrl( url + value);
  }
```

** En el html aplicamos el pipe: **

```
<td>
    <iframe [src]=" valores.uri | domseguro "  frameborder="0"></iframe>
</td>

```


---

## Servicios REST


** Peticiones HTTP **

Para poder realizar peticiones, en app.modules.ts importamos 

`import { HttpClientModule } from '@angular/common/http';`

mas abajo en imports

`HttpClientModule`


** Creamos un nuevo servicio en Angular **

`ng generate service services/spotify`

** Importamos el HttpClient en el componente deseado **

`import { HttpClient } from '@angular/common/http';`

** Importamos el servicio en el componente deseado **

`import { SpotifyService } from '../../services/spotify.service';`


Agregamos ambos en el constructor y llamamos a la funcion del servicio

```
resultado: any[] = [];

  constructor( private http: HttpClient, private peticiones: PruebaPeticionesService) {

    this.peticiones.getNewReleases()
        .subscribe( (data: any) => {
              this.resultado = data.data;
              console.log(this.resultado);
              });
  }
```

En el SERVICIO importamos


```
import { HttpClient, HttpHeaders } from '@angular/common/http';

constructor( private http: HttpClient) {

  }
```

y creamos una funcion para obtener la data

```
getNewReleases() {

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQCIErs8sv5JDs7OebxX_Iv5JDs7OebxX_Z'
    });

    return this.http.get('https://reqres.in/api/users?page=2', {headers})
              
  }
```

** Utilizamos el ngFor para mostrarlo por pantalla **

```
<ul>
    <li *ngFor="let valores of resultado; index as i"> {{ i }} {{ valores.first_name }}</li>
</ul>
```

---

## Utilizando el operador MAP

** Importamos el operador map en el SERVICIO desde el paquete con operadores reactivos: **


`import{map} from 'rxjs/operators';`


** Creamos la funcion que contiene el codigo unico para el resto de funciones: **

** Creamos la funcion para obtener la data: **
** Albums es parte del conjunto del array: **


```
constructor(private http: HttpClient) { }

  getQuery( query:string ){

    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBNbSjHQ4akStkpqYrO9twAFJuleK96CewllD68N-NTuFK0iXC84aMbOsNdr9jaPP53UMLClofaWDcNiWdBh8o1mwEkuXBUBbp2C90KhT-gmV5USRGBsPk3vo-s719QBM6s_F59YJBgaCbcrQ'
    });

    return this.http.get( url, { headers } );
  }

  getNewReleases() {

    return this.getQuery('browse/new-releases?limit=20')
               .pipe( map( data => {
                  return data['albums'].items;
                }));               
  }

```

** En el componente agregamos (loading se ejecuta hasta que este todo cargado): **

```
  nuevasCanciones: any[] = [];
  loading = true;
  

  constructor( private http: HttpClient, private peticiones: PruebaPeticionesService) {

    this.peticiones.getNewReleases()
        .subscribe( ( data: any ) => {
          this.nuevasCanciones = data;
          console.log(this.nuevasCanciones);
          this.loading = false;
    });
          
  }
```

** Finalmente, en el HTML: **

```
  <div class="card-columns m-5 animated fadeIn puntero">

    <div *ngFor="let valores of nuevasCanciones" class="card">
        <img class="card-img-top" src="{{valores.images | limpiarImagenes}}" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">{{valores.name}}</h5>
            <p class="card-text" *ngFor="let resultado of valores.artists">
                <span class="badge badge-pill badge-primary">{{resultado.name}}</span>
            </p>
        </div>
    </div>

</div>

```

---

## Mostrar errores de la API mediante subscribe



---

# Estilos de Angular

---


## NgStyle

** agregar estilos a un solo atributo html: **

```
<p [style.fontSize.px]="tamano">

    Algo de texto
    
</p>


<button (click)="tamano = tamano + 50"> Mas grande</button>

```

---

---


## NgClass

** Podemos aplicar una clase con distintas propiedades a uno o varios elementos html **
** Ver mas funciones en la documentacion Angular **
```
<div class="alert" [ngClass]="alerta" role="alert">

    Este es el texto que cambia de color
    
</div>

<button (click)="colorificacion()">Colorear</button>
```


```
alerta: string;

  colorificacion() {

      const color = prompt('Please enter your name');
  
      if (color === 'verde') {
  
        this.alerta = 'alert-success';
  
      } else if(color === 'rojo') {
  
        this.alerta = 'alert-danger';
  
      }

  }
```


---


## Directivas personalizadas

** Creamos la directiva con Angular CLI **

`ng generate directive directives/resaltado`






---

## Gráficas con Bootsrap y Angular

** Utilizando la tecnología **

** https://valor-software.com/ng2-charts/ **

Seguir los pasos de instalacion, excepto para el script que accedemos al CDN.

1. Creamos el componente para el grafico
2. Agegamos la parte de html
3. Agregamos la parte funcional al componente


---

## Generando los archivos finales para el servidor

 Instalamos http-server
 
 ** Para modo de desarrollo (Pruebas) **

1. Seleccionamo el proyoceto de Angular con la consola
2. `ng build`
3. Podemos tomar ahora la carpeta dist y ejecutar el comando `http-server -o` 
    
 ** Para modo de produccion **
 
1. Editamos el archivo enviroments.ts para cambiarlo a true.
2. Seleccionamo el proyoceto de Angular con la consola
3. `ng build --prod`
4. Podemos tomar ahora la carpeta dist y ejecutar el comando `http-server -o`




















